/*
 * Copyright 2015 Abhay Parvate <abhay.parvate@gmail.com>
 * Licensed under the Apache License, Version 2.0
 *      http://www.apache.org/licenses/LICENSE-2.0
 */

package com.wordpress.abhayparvate.dokkaiki.model;

import com.wordpress.abhayparvate.dokkaiki.model.Word;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Consumer;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

public class WordDict {
    
    private final String xmlResource = "/resources/adapted_JMdict_e.xml";
    private final HashMap <String, List <Word>> dict;
    private final HashMap <Integer, Integer> lookLeft, lookRight;
    
    public WordDict(Void v) { // For testing things other than dictionary.
        dict = new HashMap <> ();
        lookLeft = new HashMap <> ();
        lookRight = new HashMap <> ();
    }
    
    public WordDict(Consumer <Double> reporter) throws XMLStreamException, IOException {
        XMLInputFactory xif = XMLInputFactory.newFactory();
        xif.setProperty("javax.xml.stream.isNamespaceAware", false);
        xif.setProperty("javax.xml.stream.supportDTD", false);
        InputStream iStream = getClass().getResourceAsStream(xmlResource);
        int totalBytes = iStream.available();
        if (totalBytes == 0) {totalBytes = Integer.MAX_VALUE / 1000;}
        int percent = 0, oldPercent = 0;
        XMLEventReader reader = xif.createXMLEventReader(iStream);
        dict = new HashMap <> ();
        lookLeft = new HashMap <> ();
        lookRight = new HashMap <> ();
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement() &&
                    event.asStartElement().getName().getLocalPart().equals("entry")) {
                addEntry(reader);
            }
            percent = (int) ((totalBytes - iStream.available()) * 100.0 / totalBytes);
            if (percent != oldPercent) {
                oldPercent = percent;
                if (reporter != null) {reporter.accept(percent / 100.0);}
            }
        }
        reader.close();
    }
    
    private void addEntry(XMLEventReader reader) throws XMLStreamException {
        WordBuilder word = new WordBuilder();
        QName lang = new QName(null, "lang", "xml");
        while (true) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()) {
                if (event.asStartElement().getName().getLocalPart().equals("keb")) {
                    word.addKanji(getInnerText(reader));
                } else if (event.asStartElement().getName().getLocalPart().equals("reb")) {
                    word.addReading(getInnerText(reader));
                } else if (event.asStartElement().getName().getLocalPart().equals("pos")) {
                    String pos = getInnerText(reader);
                    if (!word.getPoss().contains(pos)) {
                        word.addPos(pos);
                    }
                } else if (event.asStartElement().getName().getLocalPart().equals("gloss")) {
                    if (event.asStartElement().getAttributeByName(lang) == null ||
                            event.asStartElement().getAttributeByName(lang).getValue().equals(
                                    "eng")) {
                        word.addMeaning(getInnerText(reader));
                    }
                }
            } else if (event.isEndElement()) {
                if (event.asEndElement().getName().getLocalPart().equals("entry")) {
                    break;
                }
            }
        }
        List <String> keys = new LinkedList <> ();
        keys.addAll(word.getKanji()); keys.addAll(word.getReadings());
        boolean needStem = false;
        for (String p : word.getPoss()) {
            if (p.startsWith("v1") || p.startsWith("v5")) {
                needStem = true; break;
            }
        }
        for (String ok : keys) {
            String k = (needStem) ? (ok.substring(0, ok.length() - 1)) : ok;
            if (!dict.containsKey(k)) {dict.put(k, new LinkedList <> ());}
            dict.get(k).add(word);
            int c;
            for (int i = 0; i < k.length(); i++) {
                c = k.codePointAt(i);
                if (!lookLeft.containsKey(c)) {lookLeft.put(c, 0);}
                if (!lookRight.containsKey(c)) {lookRight.put(c, 0);}
                if (lookLeft.get(c) < i) {lookLeft.put(c, i);}
                if (lookRight.get(c) < k.length() - i) {lookRight.put(c, k.length() - i);}
            }
        }
    }
    
    private String getInnerText(XMLEventReader reader) throws XMLStreamException {
        XMLEvent child = reader.nextEvent();
        return child.asCharacters().getData();
    }
    
    public List <Word> get(String text, int pos) {
        List <Word> words, tWords;
        words = new LinkedList <> ();
        if (pos >= text.length()) {return words;}
        int c = text.codePointAt(pos);
        Integer left, right;
        left = lookLeft.get(c); if (left == null) {left = 0;}
        right = lookRight.get(c); if (right == null) {right = 0;}
        for (int start = Math.max(pos - left, 0); start <= pos; start++) {
            for (int end = Math.min(text.length() - pos, right); end > 0; end--) {
                tWords = dict.get(text.substring(start, pos + end));
                if (tWords != null) {words.addAll(tWords);}
            }
        }
        return words;
    }
    
    private class WordBuilder implements Word {
        
        private final List <String> kanji;
        private final List <String> readings;
        private final List <String> meanings;
        private final List <String> poss;
        
        public WordBuilder() {
            kanji = new LinkedList <> ();
            readings = new LinkedList <> ();
            meanings = new LinkedList <> ();
            poss = new LinkedList <> ();
        }
        
        public void addKanji(String k) {kanji.add(k);}
        
        public void addReading(String r) {readings.add(r);}
        
        public void addMeaning(String m) {meanings.add(m);}
        
        public void addPos(String m) {poss.add(m);}
        
        @Override
        public List <String> getKanji() {
            return Collections.unmodifiableList(kanji);
        }

        @Override
        public List <String> getReadings() {
            return Collections.unmodifiableList(readings);
        }

        @Override
        public List <String> getMeanings() {
            return Collections.unmodifiableList(meanings);
        }
        
        @Override
        public List <String> getPoss() {
            return Collections.unmodifiableList(poss);
        }
        
    }

}
