/*
 * Copyright 2015 Abhay Parvate <abhay.parvate@gmail.com>
 * Licensed under the Apache License, Version 2.0
 *      http://www.apache.org/licenses/LICENSE-2.0
 */

package com.wordpress.abhayparvate.dokkaiki.model;

import com.wordpress.abhayparvate.dokkaiki.model.KanjiDrawing;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

public class KanjiDrawingDict {
    
    private HashMap <Integer, KanjiDrawing> dict;
    private final String xmlResource = "/resources/kanjivg-20150309.xml";
    
    public KanjiDrawingDict(Consumer <Double> reporter) throws IOException, XMLStreamException {
        XMLInputFactory xif = XMLInputFactory.newFactory();
        xif.setProperty("javax.xml.stream.isNamespaceAware", false);
        xif.setProperty("javax.xml.stream.supportDTD", false);
        InputStream iStream = getClass().getResourceAsStream(xmlResource);
        int totalBytes = iStream.available();
        if (totalBytes == 0) {totalBytes = Integer.MAX_VALUE;}
        int percent = 0, oldPercent = 0;
        XMLEventReader reader = xif.createXMLEventReader(iStream);
        dict = new HashMap <> ();
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement() &&
                    event.asStartElement().getName().getLocalPart().equals("kanji")) {
                addEntry(event, reader);
            }
            percent = (int) ((totalBytes - iStream.available()) * 100.0 / totalBytes);
            if (percent != oldPercent) {
                oldPercent = percent;
                if (reporter != null) {reporter.accept(percent / 100.0);}
            }
        }
        reader.close();
    }
    
    private void addEntry(XMLEvent pEvent, XMLEventReader reader) throws XMLStreamException {
        KanjiDrawingBuilder kdd = new KanjiDrawingBuilder();
        QName id = new QName(null, "id");
        QName d = new QName(null, "d");
        kdd.setKanji(Integer.parseInt(
                pEvent.asStartElement().getAttributeByName(id).getValue().substring(10), 16));
        while (true) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()) {
                if (event.asStartElement().getName().getLocalPart().equals("path")) {
                    kdd.addPath(event.asStartElement().getAttributeByName(d).getValue());
                }
            } else if (event.isEndElement()) {
                if (event.asEndElement().getName().getLocalPart().equals("kanji")) {
                    break;
                }
            }
        }
        if (kdd.getKanji() < 256) {return;}
        dict.put(kdd.getKanji(), kdd);
    }
        
    public KanjiDrawing get(int kanji) {return dict.get(kanji);}
    
    private class KanjiDrawingBuilder implements KanjiDrawing {
        
        private int kanji;
        private final List <String> svgPaths;
        
        public KanjiDrawingBuilder() {
            kanji = 0;
            svgPaths = new ArrayList <> ();
        }
        
        public void setKanji(int kanji) {this.kanji = kanji;}
        
        public void addPath(String path) {svgPaths.add(path);}
        
        @Override
        public int getKanji() {return kanji;}

        @Override
        public List <String> getSvgPaths() {return Collections.unmodifiableList(svgPaths);}
        
    }

}
