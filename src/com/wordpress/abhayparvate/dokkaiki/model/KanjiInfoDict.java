/*
 * Copyright 2015 Abhay Parvate <abhay.parvate@gmail.com>
 * Licensed under the Apache License, Version 2.0
 *      http://www.apache.org/licenses/LICENSE-2.0
 */

package com.wordpress.abhayparvate.dokkaiki.model;

import com.wordpress.abhayparvate.dokkaiki.model.KanjiInfo;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.function.Consumer;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 * Factory producing KanjiInfo objects, which uses the dictionary in the resource.
 */
public class KanjiInfoDict {
    
    private final String xmlResource = "/resources/adapted_kanjidic2.xml";
    private HashMap <Integer, KanjiInfo> dict;
    
    public KanjiInfoDict(Consumer <Double> reporter) throws IOException, XMLStreamException {
        XMLInputFactory xif = XMLInputFactory.newFactory();
        xif.setProperty("javax.xml.stream.isNamespaceAware", false);
        xif.setProperty("javax.xml.stream.supportDTD", false);
        InputStream iStream = getClass().getResourceAsStream(xmlResource);
        int totalBytes = iStream.available();
        if (totalBytes == 0) {totalBytes = Integer.MAX_VALUE / 1000;}
        int percent = 0, oldPercent = 0;
        XMLEventReader reader = xif.createXMLEventReader(iStream);
        dict = new HashMap <> ();
        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement() &&
                    event.asStartElement().getName().getLocalPart().equals("character")) {
                addEntry(reader);
            }
            percent = (int) ((totalBytes - iStream.available()) * 100.0 / totalBytes);
            if (percent != oldPercent) {
                oldPercent = percent;
                if (reporter != null) {reporter.accept(percent / 100.0);}
            }
        }
    }
    
    public KanjiInfo get(int kanji) {
        return dict.get(kanji);
    }
    
    private void addEntry(XMLEventReader reader) throws XMLStreamException {
        KanjiInfoBuilder kid = new KanjiInfoBuilder();
        QName r_type = new QName(null, "r_type");
        QName m_lang = new QName(null, "m_lang");
        while (true) {
            XMLEvent event = reader.nextEvent();
            StartElement start;
            if (event.isStartElement()) {
                start = event.asStartElement();
                if (start.getName().getLocalPart().equals("literal")) {
                    kid.setKanji(Character.codePointAt(getInnerText(reader), 0));
                } else if (start.getName().getLocalPart().equals("reading") &&
                        start.getAttributeByName(r_type).getValue().equals("ja_on")) {
                    kid.addOnYomi(getInnerText(reader));
                } else if (start.getName().getLocalPart().equals("reading") &&
                        start.getAttributeByName(r_type).getValue().equals("ja_kun")) {
                    kid.addKunYomi(getInnerText(reader));
                } else if (start.getName().getLocalPart().equals("meaning") &&
                        start.getAttributeByName(m_lang) == null) {
                    kid.addMeaning(getInnerText(reader));
                } else if (start.getName().getLocalPart().equals("grade")) {
                    kid.addMisc("Grade " + getInnerText(reader));
                } else if (start.getName().getLocalPart().equals("freq")) {
                    kid.addMisc("Freq " + getInnerText(reader));
                } else if (start.getName().getLocalPart().equals("jlpt")) {
                    kid.addMisc("JLPT " + getInnerText(reader));
                }
            } else if (event.isEndElement() &&
                    event.asEndElement().getName().getLocalPart().equals("character")) {
                break;
            }
        }
        dict.put(kid.getKanji(), kid);
    }
    
    private String getInnerText(XMLEventReader reader) throws XMLStreamException {
        XMLEvent child = reader.nextEvent();
        return child.asCharacters().getData();
    }
    
    private class KanjiInfoBuilder implements KanjiInfo {
        
        private int kanji;
        private final List <String> onYomi;
        private final List <String> kunYomi;
        private final List <String> meanings;
        private final List <String> misc;
        
        public KanjiInfoBuilder() {
            kanji = 0;
            onYomi = new ArrayList <> ();
            kunYomi = new ArrayList <> ();
            meanings = new ArrayList <> ();
            misc = new ArrayList <> ();
        }
        
        public void setKanji(int kanji) {this.kanji = kanji;}
        
        public void addOnYomi(String st) {onYomi.add(st);}
        
        public void addKunYomi(String st) {kunYomi.add(st);}
        
        public void addMeaning(String st) {meanings.add(st);}
        
        public void addMisc(String st) {misc.add(st);}

        @Override
        public int getKanji() {return kanji;}

        @Override
        public List<String> getOnYomi() {return Collections.unmodifiableList(onYomi);}

        @Override
        public List<String> getKunYomi() {return Collections.unmodifiableList(kunYomi);}

        @Override
        public List<String> getMeanings() {return Collections.unmodifiableList(meanings);}

        @Override
        public List<String> getMisc() {return Collections.unmodifiableList(misc);}

    }
    
}
