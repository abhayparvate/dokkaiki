/*
 * Copyright 2015 Abhay Parvate <abhay.parvate@gmail.com>
 * Licensed under the Apache License, Version 2.0
 *      http://www.apache.org/licenses/LICENSE-2.0
 */

package com.wordpress.abhayparvate.dokkaiki.model;

import java.util.List;

public interface KanjiInfo {
    
    public int getKanji();

    public List <String> getOnYomi();

    public List <String> getKunYomi();

    public List <String> getMeanings();
    
    public List <String> getMisc();
    
}
