/*
 * Copyright 2015 Abhay Parvate <abhay.parvate@gmail.com>
 * Licensed under the Apache License, Version 2.0
 *      http://www.apache.org/licenses/LICENSE-2.0
 */

package com.wordpress.abhayparvate.dokkaiki.model;

import java.util.List;

public interface Word {
    
    public List <String> getKanji();
    
    public List <String> getReadings();
    
    public List <String> getMeanings();
    
    public List <String> getPoss();
    
}
