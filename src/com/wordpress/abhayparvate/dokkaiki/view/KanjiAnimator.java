/*
 * Copyright 2015 Abhay Parvate <abhay.parvate@gmail.com>
 * Licensed under the Apache License, Version 2.0
 *      http://www.apache.org/licenses/LICENSE-2.0
 */

package com.wordpress.abhayparvate.dokkaiki.view;

import com.wordpress.abhayparvate.dokkaiki.model.KanjiDrawing;
import javafx.animation.AnimationTimer;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Slider;
import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;
import javafx.scene.shape.StrokeLineJoin;

public class KanjiAnimator extends AnimationTimer {
    
    private KanjiDrawing kanjiDrawing;
    private final GraphicsContext gContext;
    private final Slider slider;
    private long startTime;
    private int drawnStrokes;
    private boolean firstFrame;
    private boolean running;
    public boolean gridVisible;

    private final long timePerStroke = (long) 1.0e9;
    private final double gridLineWidth = 2.0;
    private final double strokeLineWidth = 3.0;
    private final double startPointWidth = 6.0;
    
    public KanjiAnimator(Canvas c, Slider slider) {
        this.slider = slider;
        gContext = c.getGraphicsContext2D();
        startTime = 0; firstFrame = true;
        running = false;
    }
    
    @Override
    public void start() {
        firstFrame = true; running = true;
        super.start();
    }
    
    @Override
    public void stop() {
        super.stop();
        running = false;
    }

    @Override
    public void handle(long now) {
        if (kanjiDrawing == null) {stop(); return;}
        if (firstFrame) {
            startTime = now; firstFrame = false; drawnStrokes = -1;
        }
        int strokes = (int) ((now - startTime) / timePerStroke);
        if (drawnStrokes >= strokes) {return;}
        drawnStrokes = strokes;
        slider.setValue(strokes % (kanjiDrawing.getSvgPaths().size() + 1));
        if (strokes > kanjiDrawing.getSvgPaths().size()) {stop();}
    }
    
    public boolean isRunning() {return running;}
    
    public void increment() {
        if (slider.getValue() >= slider.getMax()) {
            slider.setValue(slider.getMin());
        } else {
            slider.increment();
        }
    }
    
    public void decrement() {
        if (slider.getValue() <= slider.getMin()) {
            slider.setValue(slider.getMax());
        } else {
            slider.decrement();
        }
    }
    
    public void drawStage(int strokes) {
        gContext.clearRect(-5, -5, 120, 120);
        drawGrid();
        if (kanjiDrawing == null) {return;}
        if (strokes == 0) {strokes = kanjiDrawing.getSvgPaths().size() + 1;}
        for (int i = strokes; i < kanjiDrawing.getSvgPaths().size(); i++) {
            drawStroke (i, Color.LIGHTGRAY);
        }
        for (int i = 0; i < strokes - 1; i++) {
            drawStroke(i, Color.BLACK);
        }
        drawStroke(strokes - 1, Color.RED);
        drawStartPoint(strokes - 1);
    }
    
    public void set(KanjiDrawing kd) {
        stop();
        kanjiDrawing = kd;
        slider.setValue(0);
        if (kd == null) {
            slider.setMax(1);
        } else {
            slider.setMax((double)(kanjiDrawing.getSvgPaths().size()));
        }
        drawStage(0);
    }
    
    private void drawGrid() {
        if (!gridVisible) {return;}
        gContext.setStroke(Color.BURLYWOOD);
        gContext.setLineWidth(gridLineWidth);
        for (int i = 0; i <= 110; i += 55) {
            gContext.strokeLine(i, 0, i, 110);
            gContext.strokeLine(0, i, 110, i);
        }
    }
    
    private void drawStroke(int index, Color c) {
        if (kanjiDrawing == null) {return;}
        if (index < 0 || index >= kanjiDrawing.getSvgPaths().size()) {return;}
        gContext.setLineWidth(strokeLineWidth);
        gContext.setLineCap(StrokeLineCap.ROUND);
        gContext.setLineJoin(StrokeLineJoin.ROUND);
        gContext.setStroke(c);
        gContext.beginPath();
        gContext.appendSVGPath(kanjiDrawing.getSvgPaths().get(index));
        gContext.stroke();
    }
    
    private void drawStartPoint(int index) {
        if (kanjiDrawing == null) {return;}
        if (index < 0 || index >= kanjiDrawing.getSvgPaths().size()) {return;}
        double startX; double startY;
        String path = kanjiDrawing.getSvgPaths().get(index);
        int i;
        if (path.length() < 1) {return;}
        if (!"M".equals(path.substring(0,1))) {return;}
        String[] tokens = path.split("[A-Za-z ,]+");
        if (tokens.length < 3) {return;}
        try {
            startX = Double.parseDouble(tokens[1]);
            startY = Double.parseDouble(tokens[2]);
        } catch (Exception ex) {
            return;
        }
        gContext.setFill(Color.BLUE);
        gContext.fillOval(startX - startPointWidth/2, startY - startPointWidth/2,
                startPointWidth, startPointWidth);
    }

}
