/*
 * Copyright 2015 Abhay Parvate <abhay.parvate@gmail.com>
 * Licensed under the Apache License, Version 2.0
 *      http://www.apache.org/licenses/LICENSE-2.0
 */

package com.wordpress.abhayparvate.dokkaiki.view;

import javafx.scene.control.ListCell;
import javafx.scene.text.FontSmoothingType;
import javafx.scene.text.Text;

public class TextListCell extends ListCell <String> {
    
    @Override
    public void updateItem(String item, boolean empty) {
        super.updateItem(item, empty);
        if (item == null || empty) {setGraphic(null); return;}
        Text te = new Text(item);
        te.wrappingWidthProperty().bind(getListView().widthProperty().subtract(32));
        setGraphic(te);
    }

}
