/*
 * Copyright 2015 Abhay Parvate <abhay.parvate@gmail.com>
 * Licensed under the Apache License, Version 2.0
 *      http://www.apache.org/licenses/LICENSE-2.0
 */

package com.wordpress.abhayparvate.dokkaiki.view;

import com.wordpress.abhayparvate.dokkaiki.model.Word;
import com.wordpress.abhayparvate.dokkaiki.view.TextListCell;
import com.wordpress.abhayparvate.dokkaiki.view.KanjiInfoView;
import com.wordpress.abhayparvate.dokkaiki.view.InfoStage;
import java.util.List;
import javafx.scene.Parent;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class TopView {
    
    private final GridPane gridPane;
    private final TextArea inputTextArea;
    private final ListView <String> wordView;
    private final KanjiInfoView kanjiInfoView;
    private final Canvas kanjiDrawingView;
    private final Slider kanjiSlider;
    private final Label status;
    private final ProgressBar progress;
    private final Button aboutButton, helpButton, settingsButton;
    private final CheckBox gridToggle;
    private final Stage aboutStage, helpStage;
    private final SettingsStage settingsStage;

    private final String aboutTextResource = "/resources/about.txt";
    private final String helpTextResource = "/resources/help.txt";
    
    public TopView() {
        gridPane = new GridPane();
        inputTextArea = new TextArea("読解器へようこそ！");
        inputTextArea.setWrapText(true);
        wordView = new ListView <> ();
        wordView.setCellFactory((v) -> {return new TextListCell();});
        kanjiInfoView = new KanjiInfoView();
        kanjiDrawingView = new Canvas(150, 150);
        kanjiDrawingView.getGraphicsContext2D().translate(20, 20);
        gridToggle = new CheckBox();
        kanjiSlider = new Slider(0, 1, 0);
        kanjiSlider.setMajorTickUnit(5);
        kanjiSlider.setMinorTickCount(4);
        kanjiSlider.setShowTickMarks(true);
        kanjiSlider.setSnapToTicks(true);
        kanjiSlider.setBlockIncrement(1);
        status = new Label();
        progress = new ProgressBar(0);
        status.setGraphic(progress);
        progress.setVisible(false);
        aboutStage = new InfoStage(aboutTextResource);
        aboutStage.setTitle("About 読解器");
        helpStage = new InfoStage(helpTextResource);
        helpStage.setTitle("読解器 Help");
        settingsStage = new SettingsStage();
        aboutButton = new Button("About");
        helpButton = new Button("Help");
        settingsButton = new Button("Settings");
        aboutButton.setOnAction((e) -> {aboutStage.show();});
        helpButton.setOnAction((e) -> {helpStage.show();});
        settingsButton.setOnAction((e) -> {settingsStage.show();});
        arrangeGridPane();
    }
    
    public SettingsStage getSettingsStage() {
        return settingsStage;
    }
    private void arrangeGridPane() {
        HBox statuxBox = new HBox();
        VBox kanjiBoxV = new VBox();
        HBox kanjiBoxH = new HBox(10);
        Region strut = new Region();
        VBox.setVgrow(strut, Priority.ALWAYS);
        HBox.setHgrow(kanjiSlider, Priority.ALWAYS);
        kanjiBoxH.getChildren().addAll(kanjiSlider, gridToggle);
        kanjiBoxV.getChildren().addAll(kanjiDrawingView, strut, kanjiBoxH);
        statuxBox.getChildren().addAll(helpButton, aboutButton, settingsButton);
        aboutButton.prefWidthProperty().bind(statuxBox.widthProperty().multiply(1));
        helpButton.prefWidthProperty().bind(statuxBox.widthProperty().multiply(1));
        settingsButton.prefWidthProperty().bind(statuxBox.widthProperty().multiply(1));
        gridPane.add(inputTextArea, 0, 0);
        gridPane.add(kanjiInfoView.getParent(), 1, 0);
        gridPane.add(wordView, 0, 1);
        gridPane.add(kanjiBoxV, 1, 1);
        gridPane.add(status, 0, 2);
        gridPane.add(statuxBox, 1, 2);
        RowConstraints row0, row1, row2;
        ColumnConstraints col0, col1;
        row0 = rowPercent(65); row1 = rowPercent(30); row2 = rowPercent(5);
        gridPane.getRowConstraints().addAll(row0, row1, row2);
        col0 = colPercent(80); col1 = colPercent(20);
        gridPane.getColumnConstraints().addAll(col0, col1);
    }
    
    private RowConstraints rowPercent(double p) {
        RowConstraints row = new RowConstraints();
        row.setPercentHeight(p);
        return row;
    }
    
    private ColumnConstraints colPercent(double p) {
        ColumnConstraints col = new ColumnConstraints();
        col.setPercentWidth(p);
        return col;
    }
    
    public Parent getTop() {return gridPane;}
    
    public TextArea getInputTextArea() {return inputTextArea;}
    
    public KanjiInfoView getKanjiInfoView() {return kanjiInfoView;}
    
    public Canvas getDrawingArea() {return kanjiDrawingView;}
    
    public Slider getKanjiSlider() {return kanjiSlider;}
    
    public CheckBox getGridToggle() {return gridToggle;}
    
    public ProgressBar getProgress() {return progress;}

    public ListView<String> getWordView() {
        return wordView;
    }
    
    public void setWordViewContents(List <Word> words) {
        wordView.getItems().clear();
        if (words == null) {return;}
        for (Word word : words) {
            wordView.getItems().add(
                    String.join(" / ",
                        String.join(", ", word.getKanji()),
                        String.join(", ", word.getReadings()),
                        String.join(", ", word.getPoss()),
                        String.join("; ", word.getMeanings())));
        }
    }
    
    public void setStatus(String st) {status.setText(st);}

}
