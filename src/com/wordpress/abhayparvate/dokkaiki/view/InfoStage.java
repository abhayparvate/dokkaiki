/*
 * Copyright 2015 Abhay Parvate <abhay.parvate@gmail.com>
 * Licensed under the Apache License, Version 2.0
 *      http://www.apache.org/licenses/LICENSE-2.0
 */

package com.wordpress.abhayparvate.dokkaiki.view;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Background;
import javafx.scene.text.Text;
import javafx.scene.text.TextFlow;
import javafx.stage.Stage;

public class InfoStage extends Stage {
    
    public InfoStage(String resource) {
        super();
        String contents = readResource(resource);
        ScrollPane sp = new ScrollPane();
        sp.setPadding(new Insets(10));
        TextFlow textFlow = new TextFlow(new Text(contents));
        textFlow.prefWidthProperty().bind(
                sp.widthProperty().subtract(sp.prefViewportWidthProperty()).subtract(40));
        sp.setContent(textFlow);
        setScene(new Scene(sp, 500, 400));
    }
    
    private String readResource(String resource) {
        try {
            BufferedReader input = new BufferedReader(new InputStreamReader(
                    getClass().getResourceAsStream(resource)));
            StringBuilder sb = new StringBuilder();
            int c;
            while ((c = input.read()) != -1) {
                sb.append((char)c);
            }
            input.close();
            return sb.toString();
        } catch (Exception ex) {
            return ("Internal error: cannot read resource: " + resource);
        }
    }

}
