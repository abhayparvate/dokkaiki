/*
 * Copyright 2015 Abhay Parvate <abhay.parvate@gmail.com>
 * Licensed under the Apache License, Version 2.0
 *      http://www.apache.org/licenses/LICENSE-2.0
 */

package com.wordpress.abhayparvate.dokkaiki.view;

import com.wordpress.abhayparvate.dokkaiki.model.KanjiInfo;
import javafx.scene.Parent;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;

public class KanjiInfoView {
    
    private final ListView <String> container;

    public ListView<String> getContainer() {
        return container;
    }
    
    public KanjiInfoView() {
        container = new ListView <> ();
        container.setCellFactory((v) -> {return new TextListCell();});
    }
    
    public Parent getParent() {return container;}
    
    public void set(KanjiInfo ki) {
        container.getItems().clear();
        if (ki == null) {return;}
        container.getItems().add(new String(Character.toChars(ki.getKanji())));
        container.getItems().add(String.join("; ", ki.getOnYomi()));
        container.getItems().add(String.join("; ", ki.getKunYomi()));
        container.getItems().add(String.join("; ", ki.getMeanings()));
        container.getItems().add(String.join("; ", ki.getMisc()));
    }
    
    private Label makeLabel() {
        Label la = new Label();
        la.setPrefWidth(200);
        la.setWrapText(true);
        // la.setPadding(new Insets(5));
        return la;
    }

}
