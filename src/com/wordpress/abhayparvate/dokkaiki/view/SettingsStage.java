/*
 * Copyright 2018 Abhay Parvate <abhay.parvate@gmail.com>
 * Licensed under the Apache License, Version 2.0
 *      http://www.apache.org/licenses/LICENSE-2.0
 */

package com.wordpress.abhayparvate.dokkaiki.view;

import javafx.geometry.Orientation;
import javafx.scene.Scene;
import javafx.scene.control.ListView;
import javafx.scene.control.Slider;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;

import javafx.stage.Stage;

public class SettingsStage extends Stage {
    
    private final ListView <String> fontFamilyChoice;
    private final Slider fontSizeSlider;

    public ListView <String> getFontFamilyChoice() {
        return fontFamilyChoice;
    }
    
    public Slider getFontSizeSlider() {
        return fontSizeSlider;
    }

    public SettingsStage() {
        super();
        HBox hbox = new HBox();
        fontFamilyChoice = new ListView <> ();
        fontFamilyChoice.getItems().addAll(Font.getFamilies());
        fontFamilyChoice.getSelectionModel().select(Font.getDefault().getFamily());
        fontFamilyChoice.scrollTo(fontFamilyChoice.getSelectionModel().getSelectedIndex());
        fontSizeSlider = new Slider(5.0, 25.0, Font.getDefault().getSize());
        fontSizeSlider.setOrientation(Orientation.VERTICAL);
        fontSizeSlider.setMajorTickUnit(5.0);
        fontSizeSlider.setMinorTickCount(4);
        fontSizeSlider.setSnapToTicks(true);
        fontSizeSlider.setShowTickLabels(true);
        fontSizeSlider.setShowTickMarks(true);
        fontSizeSlider.setBlockIncrement(1.0);
        hbox.getChildren().addAll(fontFamilyChoice, fontSizeSlider);
        setTitle("Font family and size");
        setScene(new Scene(hbox));
    }

}
