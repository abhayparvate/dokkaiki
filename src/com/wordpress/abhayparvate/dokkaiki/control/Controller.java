/*
 * Copyright 2015 Abhay Parvate <abhay.parvate@gmail.com>
 * Licensed under the Apache License, Version 2.0
 *      http://www.apache.org/licenses/LICENSE-2.0
 */

package com.wordpress.abhayparvate.dokkaiki.control;

import com.wordpress.abhayparvate.dokkaiki.view.KanjiAnimator;
import com.wordpress.abhayparvate.dokkaiki.model.KanjiDrawingDict;
import com.wordpress.abhayparvate.dokkaiki.model.KanjiInfoDict;
import com.wordpress.abhayparvate.dokkaiki.view.KanjiInfoView;
import com.wordpress.abhayparvate.dokkaiki.view.TopView;
import com.wordpress.abhayparvate.dokkaiki.model.WordDict;
import java.io.IOException;
import javafx.animation.AnimationTimer;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Slider;
import javafx.scene.control.TextArea;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Font;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

public class Controller extends Application {
    
    @Override
    public void start(Stage primaryStage)
            throws ParserConfigurationException, SAXException, IOException {
        TopView view = new TopView();
        Scene scene = new Scene(view.getTop());
        primaryStage.setTitle("読解器");
        primaryStage.setScene(scene);
        setup(view);
        Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());        
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
    
    private KanjiInfoDict kanjiInfoDict;
    private KanjiDrawingDict kanjiDrawingDict;
    private WordDict wordDict;
    private TopView top;
    private TextArea textArea;
    private KanjiInfoView kanjiInfoView;
    private KanjiAnimator kanjiAnimator;
    private Canvas kanjiDrawingArea;
    private Slider kanjiSlider;
    private CheckBox gridToggle;
    private DelayedUpdater updater;
    
    private String fontFamily = Font.getDefault().getFamily();
    private int fontSize = (int) Font.getDefault().getSize();
    
    private final long updateDelay = (long) 0.7e9;
    
    private void setup(TopView top) throws ParserConfigurationException, SAXException, IOException {
        this.top = top;
        kanjiInfoDict = null;
        kanjiDrawingDict = null;
        wordDict = null;
        textArea = top.getInputTextArea();
        kanjiInfoView = top.getKanjiInfoView();
        kanjiDrawingArea = top.getDrawingArea();
        kanjiSlider = top.getKanjiSlider();
        kanjiAnimator = new KanjiAnimator(kanjiDrawingArea, kanjiSlider);
        kanjiSlider.valueProperty().addListener((ev, ov, nv) -> {
            kanjiAnimator.drawStage(nv.intValue());
        });
        gridToggle = top.getGridToggle();
        gridToggle.selectedProperty().addListener((ev, ov, nv) -> {
            kanjiAnimator.gridVisible = nv;
            kanjiAnimator.drawStage((int) kanjiSlider.getValue());
        });
        updater = new DelayedUpdater();
        textArea.setOnKeyPressed((keyEvent) -> {handleKeyEvent(keyEvent);});
        textArea.textProperty().addListener((ev, old, text) -> {updater.start();});
        textArea.caretPositionProperty().addListener((ev, old, pos) -> {updater.start();});
        kanjiDrawingArea.setOnMouseClicked((e) -> {kanjiAnimator.start();});
        top.getSettingsStage().getFontFamilyChoice().getFocusModel().focusedItemProperty().
            addListener((ev, old, newFontFamily) -> {
                fontFamily = newFontFamily;
                setFont();
            });
        top.getSettingsStage().getFontSizeSlider().valueProperty().
            addListener((ev, old, newFontSize) -> {
                fontSize = newFontSize.intValue();
                setFont();
            });
        updateView();
        startLoader();
    }
    
    private void setFont() {
        String fs = "-fx-font-family: '" + fontFamily +
                "'; -fx-font-size: " + fontSize + "pt;";
        textArea.setStyle(fs);
        top.getWordView().setStyle(fs);
        kanjiInfoView.getContainer().setStyle(fs);
    }
    
    private void startLoader() {
        Runnable loader = () -> {
            try {
                Platform.runLater(() -> {top.getProgress().setVisible(true);});
                WordDict wd = new WordDict((p) -> {
                    Platform.runLater(() -> {
                        top.setStatus("Loading dictionary");
                        top.getProgress().setProgress(p);
                    });
                });
                Platform.runLater(() -> {wordDict = wd; updateView();});
                KanjiInfoDict kid = new KanjiInfoDict((p) -> {
                    Platform.runLater(() -> {
                        top.setStatus("Loading Kanji Info");
                        top.getProgress().setProgress(p);
                    });
                });
                Platform.runLater(() -> {kanjiInfoDict = kid; updateView();});
                KanjiDrawingDict kdd = new KanjiDrawingDict((p) -> {
                    Platform.runLater(() -> {
                        top.setStatus("Loading drawings");
                        top.getProgress().setProgress(p);
                    });
                });
                Platform.runLater(() -> {kanjiDrawingDict = kdd; updateView();});
                Platform.runLater(() -> {top.setStatus(""); top.getProgress().setVisible(false);});
            } catch (Exception ex) {
                ex.printStackTrace();
                Platform.runLater(() -> {top.setStatus("Error in loading");});
            }
        };
        Thread t = new Thread(loader);
        t.setDaemon(true);
        t.start();
    }
    
    private void updateView() {
        updateInfoView();
        updateDrawingView();
        updateWordView();
    }
    
    private void updateInfoView() {
        if (kanjiInfoDict == null) {return;}
        String t = textArea.getText();
        int pos = textArea.getCaretPosition();
        if (pos >= t.length()) {
            kanjiInfoView.set(null);
        } else {
            kanjiInfoView.set(kanjiInfoDict.get(t.codePointAt(pos)));
        }
    }
    
    private void updateDrawingView() {
        if (kanjiDrawingDict == null) {return;}
        String t = textArea.getText();
        int pos = textArea.getCaretPosition();
        if (pos >= t.length()) {
            kanjiAnimator.set(null);
        } else {
            kanjiAnimator.set(kanjiDrawingDict.get(t.codePointAt(pos)));
        }
    }
    
    private void updateWordView() {
        if (wordDict == null) {return;}
        String t = textArea.getText();
        int pos = textArea.getCaretPosition();
        top.setWordViewContents(wordDict.get(t, pos));
    }
    
    private void handleKeyEvent(KeyEvent key) {
        KeyCode code = key.getCode();
        if (code.equals(KeyCode.F2)) {
            if (kanjiAnimator.isRunning() || kanjiSlider.getValue() != 0) {
                kanjiAnimator.stop(); kanjiSlider.adjustValue(0);
            }
            else {kanjiAnimator.start();}
        } else if (code.equals(KeyCode.F3)) {
            kanjiAnimator.stop(); kanjiAnimator.increment();
        } else if (code.equals(KeyCode.F4)) {
            kanjiAnimator.stop(); kanjiAnimator.decrement();
        } else if (code.equals(KeyCode.F5)) {
            gridToggle.setSelected(!gridToggle.isSelected());
        }
    }
    
    private class DelayedUpdater extends AnimationTimer {
        
        private boolean justStarted = true;
        private long startTime;

        @Override
        public void handle(long now) {
            if (justStarted) {startTime = now; justStarted = false;}
            if (now - startTime > updateDelay) {
                updateView(); stop();
            }
        }
        
        @Override
        public void start() {
            justStarted = true;
            super.start();
        }
        
        @Override
        public void stop() {
            super.stop();
        }
        
    }

}
