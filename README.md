# Dokkaiki - 読解器 - Learn to read Japanese #

## Welcome ##

Learning the Japanese language is a multifaceted activity: For a better understanding, one needs to learn not only words, but individual kanji characters with their multiple readings and meanings.

Depending upon your learning style, it is best done in an integrated manner. When reading some text, it is a good idea to not just find out the meaning of a word, but also the constituent kanji, at the same time. This can help some contextual connections in our minds. It is even better if one practices writing those kanji at the same time.

Dokkaiki precisely serves this purpose, if this style suits you. One just needs to paste in the text to be studied. All the above aspects of information related to a word are presented in one screen, when you move the caret (not the mouse pointer) to the word. See more in the "Using dokkaiki" section below.

Of course this is not for absolute beginners of Japanese; One needs a little familiarity with the language before jumping into this type of learning. In particular, dokkaiki assumes some elementary knowledge of Japanese, such as some aspects of sentence structure, ability to read hiragana and katakana, etc.

## How do I get set up? ##

Dokkaiki is written in Java, and is currently distributed as a jar file. This means that you will need the Java Runtime Environment 8 to run it. You can get it at:
    http://www.oracle.com/technetwork/java/javase/downloads/index.html

You only need the JRE. The other things on the above page, such as Server JRE, or JDK, are not needed. Make sure that you choose JRE 8.

After that is installed, nothing else is needed except the dokkaiki-n-m.jar file. You can get it at the [Downloads](https://bitbucket.org/abhayparvate/dokkaiki/downloads) page. (The combination n-m stands for the version. For example version 1.0 file has the name "dokkaiki-1-0.jar".) Unless you have a specific reason, choose the latest for download.

That's it! Now you can start the program in the usual way. For example on MS Windows, it just means double-clicking the downloaded dokkaiki-n-m.jar file. It will start the program provided your Java installation was done to automatically handle jar files (which is usually the default).

## Using dokkaiki ##

The left top big box is a text entry area. This is where you type or paste Japanese text with which you need reading help. As and when you encounter words which you don't know, you can move the caret over that word. The meanings of the possible words under the caret are displayed in the area below the input area.

Since Japanese writing does not contain spaces, a program cannot be sure of where a word starts, or how long it is. Dokkaiki uses some simple heuristics derived from the dictionary data to figure out possible words containing the character at the caret, and presents them in a best-guess-first order.

On the right side you will see the information about the kanji character at the caret: its onyomi, kunyomi, and meanings.

The right bottom box shows the handwritten version of the kanji. It will animate the stroke order when you click on it.

*NOTE* It will take about half a minute to load the dictionary into memory in the beginning. But you can start pasting or entering text immediately after dokkaiki starts. The dictionary meanings will start appearing as soon as the loading is complete.

*NOTE* Dokkaiki mostly recognizes stems of inflected verbs, but may not always be successful.

## System requirements ##

Currently dokkaiki keeps all the dictionaries in memory. The memory footprint is about 300Mb. So a total memory of 1GB or above is recommended.

And yes, being written in Java, it runs on any platform for which JRE 8 is available. This of course includes a variety of versions and distributions of Linux and MS Windows.

## My reasons for developing dokkaiki ##

I live in Japan at present, and learning Japanese. I am also learning Java. Developing dokkaiki is a great combination to allow me to learn both!

You may observe occasional glitches, which are natural because I am still learning!

## Credits ##

This project would not have been possible without the immense amount of work that has gone into creating the data. Dokkaiki just puts things together; the data it uses comes from the following sources:

The [JMDict](http://www.edrdg.org/jmdict/j_jmdict.html) file (English-only version) is the property of the [Electronic Dictionary Research and Development Group](http://www.edrdg.org/), and is used in conformance with the Group's [licence](http://www.edrdg.org/edrdg/licence.html).

The [KANJIDIC2](http://www.edrdg.org/kanjidic/kanjd2index.html) file is also the property of the [Electronic Dictionary Research and Development Group](http://www.edrdg.org/), and is used in conformance with the Group's [licence](http://www.edrdg.org/edrdg/licence.html).

The [KanjiVG](http://kanjivg.tagaini.net/) stroke order diagrams data is copyright Ulrich Apel and is used according to their [licence](http://creativecommons.org/licenses/by-sa/3.0/).

I also use the Chrome/Mozzila plugins Rikaichan/Rikaikun. They certainly have influenced a lot as concepts. But no code is derived from theirs.

## Feedback ##

For any complaints, suggestions, praise, please write to me at <abhay.parvate@gmail.com>.

Please note that this is not my employment. I develop this mostly on weekends. It may take a couple of days respond to emails.

## Copyright, License and (no) Warranties ##

Dokkaiki software is Copyright 2015, Abhay Parvate <abhay.parvate@gmail.com>.

This is a free software, made available under the Apache licence, version 2.0:
    http://www.apache.org/licenses/LICENSE-2.0

This roughly means that you can use the program as you see fit.

It also means that you can also redistribute it, or you can get the source and use and/or modify the source to suit yourself and distribute that as you see fit, provided that you preserve the original copyright message.

It also means that there are no warranties.

You are encouraged to read the full and precise terms and conditions in the above licence document.